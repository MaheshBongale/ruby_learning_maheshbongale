module Adoption_methods
  def open_home_page
    @browser = Watir::Browser.new :'firefox'
  end


  def goto_adoption_site
    @browser.goto 'http://puppies.herokuapp.com'
  end

  def view_details(index_number)
    @browser.button(value: 'View Details', :index => index_number).when_present.click
  end

  def goto_adoptme_page
    @browser.button(value: 'Adopt Me!').when_present.click
  end

  def complete_adoption
    @browser.button(value: 'Complete the Adoption').when_present.click
  end

  def text_feilds(name, address, email, pay_type)
    @browser.textarea(id: 'order_name').when_present.set(name)
    @browser.textarea(id: 'order_address').when_present.set(address)
    @browser.textarea(id: 'order_email').when_present.set(email)
    @browser.select_list(id: 'order_pay_type').when_present.select(pay_type)
    @browser.button(value: 'Place Order').when_present.click
  end
end